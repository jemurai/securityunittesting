# README #

To run the security unit testing practice project, all you need to do is clone the git repo and run rake.

### What is this repository for? ###

* Use this security unit testing repo as a framework for learning unit testing for security.
* Version 1.0

### How do I use it? ###

* Clone the repo
* Run rake (rake test)
* Start by adding validation for ZIP
* Do it TDD style!
* Now do it for States
* Repeat for Names
* Repeat for Passwords

### Who do I talk to? ###

* Matt Konda or Maurice Rabb